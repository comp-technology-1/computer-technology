
/* Complete the function getFirstCharacter such that it returns the first character of the name parameter it receives. */
function getFirstCharacter(name) {
    return name[0];

}

// Sample usage - do not modify
console.log(getFirstCharacter("Amsterdam")); // "A"
console.log(getFirstCharacter("Japan")); // "J"

/******************************************************************************************************************/
/* Complete the function getLastCharacter such that it returns the last character of the name parameter it receives.
*/
function getLastCharacter(name) {
    return name[name.length - 1 ];
    //return name.at(-1)
}

// Sample usage - do not modify
console.log(getLastCharacter("Sam")); // "m"
console.log(getLastCharacter("Alex")); // "x"
console.log(getLastCharacter("Charley")); // "y"

/* substring method 
Complete the function skipFirstCharacter such that it returns all the characters except the first one from the text parameter it receives.*/

function skipFirstCharacter(text) {
    // return text.substring(1,text.length)
    return text.substring(1, text.length);
}

// Sample usage - do not modify
console.log(skipFirstCharacter("Xcode")); // "code"
console.log(skipFirstCharacter("Hello")); // "ello"