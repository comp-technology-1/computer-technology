/* length is the property which is already precomputed so not using the ()after that  */


function getCharCount(str) {
    return str.length;
}

// Sample usage - do not modify
console.log(getCharCount("Sam")); // 3
console.log(getCharCount("Alex 123")); // 8
console.log(getCharCount("Charley is here")); // 15



//********************************************************************************************************* */

/*2) Complete the function shoutMyName such that it returns the name parameter it receives all in upper case. */

function shoutMyName(name) {
    return name.toUpperCase();
}

// Sample usage - do not modify
console.log(shoutMyName("Sam")); // "SAM"
console.log(shoutMyName("Charley")); // "CHARLEY"
console.log(shoutMyName("alex")); // "ALEX"

//*******************************************************************************************************************8 */

/* Complete the function lowerName such that it returns the name parameter it receives all in lower case. */
function lowerName(name) {
    return name.toLowerCase();
}

// Sample usage - do not modify
console.log(lowerName("Sam")); // "sam"
console.log(lowerName("ALEX")); // "alex"
//************************************************************************************************************************ */
//4

